<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1460628705898" ID="ID_688994960" MODIFIED="1460650192920" TEXT="Cleansheets">
<node CREATED="1460628728845" FOLDED="true" ID="ID_916706698" MODIFIED="1460653952515" POSITION="right" TEXT="01- Core">
<node CREATED="1460628776014" ID="ID_72470044" MODIFIED="1460652786173" STYLE="fork" TEXT="Core01- Extensions Manager">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Evolve Cleansheets so that it has more advanced features for the management of its extensions.
    </p>
    <p>
      
    </p>
    <p>
      Relationships:
    </p>
    <p>
      - This FI is very important because almost all new functionality will be implement through this mechanism and it is very probable that changes in the interface may be required for a full solution.
    </p>
  </body>
</html>
</richcontent>
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<icon BUILTIN="stop-sign"/>
<icon BUILTIN="stop"/>
<node CREATED="1460629632170" ID="ID_1014241719" MODIFIED="1460651402067" TEXT="Core01.1- Activate and Deactivate Extensions">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      It should exist a new window that allows to activate and deactivate extensions of Cleansheets. A deactivated extension means that all its functionalities are disabled.
    </p>
    <p>
      
    </p>
    <p>
      Difficulty: Hard
    </p>
    <p>
      Mandatory: Yes
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="stop-sign"/>
<icon BUILTIN="stop"/>
</node>
<node CREATED="1460629646834" ID="ID_724127867" MODIFIED="1460646000792">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Core01.2-&#160;Auto-description of Extensions
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 36" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">Extensions should have associated metadata. Particularly, extensions should have a version number, a name and a description. Cleansheets should display to the user the metadata of the extensions before loading them. The user should be able to cancel the loading of an extension and also to select the version of the extension to be loaded (if there are more than one). </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 36" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            Difficulty: Hard
          </p>
          <p>
            Mandatory: Yes
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="stop-sign"/>
<icon BUILTIN="stop"/>
</node>
</node>
<node CREATED="1460628789160" ID="ID_1669806407" MODIFIED="1460652479654">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Core02- Comments
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Several functionalities relating to the comments extension of Cleansheets.
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
<node CREATED="1460629677573" ID="ID_476455161" MODIFIED="1460652991515">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Core02.1- Comments on Cells
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 36" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol start="0" style="list-style-type: none">
            <li>
              <p>
                <font face="SFTI1000">Setup extension for comments on cells. The user should be able to activate and deactivate comments on cells. When activated, a sidebar for the comments should appear. The sidebar should be composed of a simple textbox to display and edit a comment. At the moment no real comment should be persisted when the worksheet is saved. </font><font face="SFBX1000">This FI is already implemented! </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 36" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            Difficulty: Medium
          </p>
          <p>
            Mandatory: No / Already implemented
          </p>
        </div>
      </div>
    </div>
  </body>
</html>
</richcontent>
<icon BUILTIN="prepare"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1460629697210" ID="ID_1411721116" MODIFIED="1460646200745" TEXT="Core02.2- Tooltip and User Associated to Comment">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 36" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">Cleansheets should register the name of the user that creates comments and each cell should support several comments. When the mouse pointer is hovering above a cell and the cell has comments then these comments should be displayed in a form similar to a &quot;tooltip&quot;. The name of the author of each comment should also appear in all displays of comments. Comments should be persisted with the workbook. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 36" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            Difficulty: Medium
          </p>
          <p>
            Mandatory: No
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
<node CREATED="1460629722366" ID="ID_876053552" MODIFIED="1460646326707" TEXT="Core02.3- Rich Comments and History">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="SFRM1000">Cleansheets should support rich content in comments. For instance, the user should be able to apply style and format to the comments. It should also exist an history of changes. The user interface should display the history of changes to a comment and allow the user to make a new version of a comment based on an old one. It also should have a search feature, allowing the user to search for comments based on text patterns (including the history in the search). </font>
          </p>
          <div title="Page 36" class="page">
            <div class="layoutArea">
              <div class="column">
                <p>
                  
                </p>
                <p>
                  Difficulty: Medium
                </p>
                <p>
                  Mandatory: No
                </p>
              </div>
            </div>
          </div>
          <p>
            
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
</node>
<node CREATED="1460628798168" ID="ID_1812238624" MODIFIED="1460653139226">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Core03- Sort
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      A new extension to add sort functionalities to Cleansheets.
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
<node CREATED="1460641289826" ID="ID_1180132151" MODIFIED="1460646382461" TEXT="Core03.1- Column Sort">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">Sort the contents of a column of cells. It should be possible to select the order: ascending or descending. The interaction with the user can be based solely on menu options. It should be possible to sort columns with numeric and/or text contents. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 36" class="page">
            <div class="layoutArea">
              <div class="column">
                <p>
                  
                </p>
                <p>
                  Difficulty: Medium
                </p>
                <p>
                  Mandatory: No
                </p>
              </div>
            </div>
          </div>
          <p>
            
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
<node CREATED="1460641302303" ID="ID_1001790409" MODIFIED="1460646416333" TEXT="Core03.2- Sort Range of Cells">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">Sort a range of cells. A range of cells is a rectangular area delimited by an upper left corner and a lower right corner. The sorting is based on a column of the range. It should be possible to select the order: ascending or descending. Interaction with the user should be based on a popup menu. It should be possible to sort data of the following types: numeric, text or date. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 36" class="page">
            <div class="layoutArea">
              <div class="column">
                <p>
                  
                </p>
                <p>
                  Difficulty: Medium
                </p>
                <p>
                  Mandatory: No
                </p>
              </div>
            </div>
          </div>
          <p>
            
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
<node CREATED="1460641317770" ID="ID_1813703203" MODIFIED="1460646463317" TEXT="Core03.3- Auto-Sorting">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">Automatically sort a range of cells. After defining a range of cells like in the previous FI Cleansheets should automatically maintain the order as the user updates the contents of the range. Visual marks should be displayed for the range of cells as well as visual indicators for the sorting column and sorting order. The user should be able to change the sorting column by clicking on a new column. Clicking on the sorting column should change the sorting order. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 36" class="page">
            <div class="layoutArea">
              <div class="column">
                <p>
                  
                </p>
                <p>
                  Difficulty: Medium
                </p>
                <p>
                  Mandatory: No
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
</node>
<node CREATED="1460628802739" ID="ID_970709812" MODIFIED="1460653336453">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Core04- Navigation
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cleansheets should have a new &quot;navigation&quot; window. This navigation window displays a tree-like vision of the contents of Cleansheets to the user.
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="stop"/>
<node CREATED="1460641358094" ID="ID_663510385" MODIFIED="1460646546635" TEXT="Core04.1- Navigation Window">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000">Cleansheets should have a navigation window (sidebar). The navigation should display the following contents: workbooks; spreadsheets; non-empty cells; formulas; values. A double click in one of the elements should update the mouse focus to show that element. The contents of the navigator should be automatically updated. </font>
              </p>
            </li>
          </ol>
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <div title="Page 36" class="page">
                  <div class="layoutArea">
                    <div class="column">
                      <p>
                        Difficulty: Medium
                      </p>
                      <p>
                        Mandatory: No
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <p>
            
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
<node CREATED="1460641380651" ID="ID_941716021" MODIFIED="1460646910412" TEXT="Core04.2- Extensions in Navigator">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000">The navigator should now include information regarding the extensions. Active and inactive extensions should appear in the navigator. For each extension the navigator should display its properties: menus; cell extension; sidebar, etc. When feasible, a double click should change the focus of the mouse to the clicked element. </font>
              </p>
            </li>
            <li>
              
            </li>
            <li>
              <div title="Page 37" class="page">
                <div class="layoutArea">
                  <div class="column">
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 36" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <p>
                                  Difficulty: Hard
                                </p>
                                <p>
                                  Mandatory: No
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div title="Page 36" class="page">
                <div class="layoutArea">
                  <div class="column">
                    <p>
                      Relationships:
                    </p>
                  </div>
                </div>
              </div>
              (Core01)- This FI it has to load several data from extensions that may not be easy to capture. &#160;Therefore, it is specially linked with Core01 and special attention should be given to the extension's interface.
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="stop"/>
<icon BUILTIN="broken-line"/>
</node>
<node CREATED="1460641394902" ID="ID_253714303" MODIFIED="1460653938759" TEXT="Core04.3- Extensible Navigator">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol start="0" style="list-style-type: none">
            <li>
              <p>
                <font face="SFRM1000">The Navigator feature should be extensible. Extensions of Cleansheets should be able to extend the navigator with specific content. These content should be displayed in a branch of the navigator tree. The features Core04.1 and Core04.2 should be provided as navigator extensions. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol start="0" style="list-style-type: none">
            <li>
              <div title="Page 37" class="page">
                <div class="layoutArea">
                  <div class="column">
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 36" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <p>
                                  Difficulty: Hard
                                </p>
                                <p>
                                  Mandatory: No
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div title="Page 38" class="page">
                <div class="layoutArea">
                  <div class="column">
                    <ol start="0" style="list-style-type: none">
                      <li>
                        <p>
                          <span style="font-family: SFBX1000; font-size: 10.000000pt"><font size="10.000000pt" face="SFBX1000">Relationships</font></span><span style="font-family: SFRM1000; font-size: 10.000000pt"><font size="10.000000pt" face="SFRM1000">: (Core01) This FI requires redesign of the extensions interface and therefore special attention should be given to its design and implications in other features. </font></span>
                        </p>
                      </li>
                    </ol>
                  </div>
                </div>
              </div>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html>
</richcontent>
<icon BUILTIN="stop"/>
<icon BUILTIN="broken-line"/>
</node>
</node>
<node CREATED="1460628813720" ID="ID_1385749714" MODIFIED="1460653463165">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Core05- Email
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      An extension that adds email support to Cleansheets.
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
<node CREATED="1460641409322" ID="ID_13874632" MODIFIED="1460647220499" TEXT="Core05.1- Email Configuration">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="SFRM1000">The new extension should have a window to setup email. This window should be used to setup the required configurations for email. For instance, the account data and server to be used to send emails. All the configuration data should be saved in a proper file (used to save global data). The window should have a button to send a test email. This test email should get its contents (destination, subject and body) from the contents of specific cells (to be selected when the user select the test email button). The window should display a preview of the email and the result of the test. </font>
          </p>
          <p>
            
          </p>
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 37" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <div title="Page 36" class="page">
                                  <div class="layoutArea">
                                    <div class="column">
                                      <p>
                                        Difficulty: Medium
                                      </p>
                                      <p>
                                        Mandatory: No
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ol>
              </div>
            </div>
          </div>
          <p>
            
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
<node CREATED="1460641416513" ID="ID_880064479" MODIFIED="1460647250328" TEXT="Core05.2- Send Email and Outbox">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 38" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">The new extension should now include an option to send an email. The window should include at least the following fields: destination; subject and body. It should be possible to include in the body of the message the contents of a range of cells. Also, it should exist a new sidebar window that will display the outbox, i.e., the historical of all sent messages. In the outbox window, when the user double clicks a message, its contents should be displayed (in a window similar to the send window). </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 37" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <div title="Page 36" class="page">
                                  <div class="layoutArea">
                                    <div class="column">
                                      <p>
                                        Difficulty: Medium
                                      </p>
                                      <p>
                                        Mandatory: No
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
<node CREATED="1460641437476" ID="ID_805915783" MODIFIED="1460647284730" TEXT="Core05.3- Batch Email and Inbox">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 38" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">The extension should now support an option to send a batch of emails, enabling the send of several emails based on the contents of a range o cells. The range of cells should have 4 columns: destination, subject, contents and result. The first 3 columns should have data before sending the batch email. The last column should be updated by Cleansheets with the result of each email as the emails are being sent. These emails should also appear in the outbox. The extension should now include an inbox with incoming messages. A double click in a message in the inbox should also display the details of the received email. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 37" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <div title="Page 36" class="page">
                                  <div class="layoutArea">
                                    <div class="column">
                                      <p>
                                        Difficulty: Medium
                                      </p>
                                      <p>
                                        Mandatory: No
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
</node>
<node CREATED="1460628820749" ID="ID_293020401" MODIFIED="1460653629569">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Core06- Images
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      An extension that adds support for image manipulation in Cleansheets.
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
<node CREATED="1460641478558" ID="ID_927689803" MODIFIED="1460647364089" TEXT="Core06.1- Insert Image">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 38" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">The extension should include an option to insert an image. The inserted image should become associated with the active/selected cell. It should also exist a new sidebar window to display the images that are associated with the current cell (in a manner similar to how comments work). A cell can have several associated images. The sidebar should have an option (button) to remove/delete images. The workbook should only save links to the files that contain the images. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 37" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <div title="Page 36" class="page">
                                  <div class="layoutArea">
                                    <div class="column">
                                      <p>
                                        Difficulty: Medium
                                      </p>
                                      <p>
                                        Mandatory: No
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
<node CREATED="1460641489588" ID="ID_1356611022" MODIFIED="1460647421099" TEXT="Core06.2- Overlay Image Window">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 38" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">Cleansheets should display a small icon in the corner of cells that have associated images. When the mouse is hovering such cells a modeless window (i.e., an overlay window) should appear to display the associated images (this is independent from the sidebar window). The window should always appear next to the cell. This window should have buttons to browse over all the images. When the mouse focus leaves the cell the window should automatically close. The images should also be persisted with (i.e., inside) the workbook. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 37" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <div title="Page 36" class="page">
                                  <div class="layoutArea">
                                    <div class="column">
                                      <p>
                                        Difficulty: Medium
                                      </p>
                                      <p>
                                        Mandatory: No
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
<node CREATED="1460641500285" ID="ID_104011785" MODIFIED="1460647498915" TEXT="Core06.3- Persisting Images">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 38" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">When a image is inserted its contents should be imported to the workbook. The images should also be persisted with (i.e., inside) the workbook. The window overlay that displays images should now be able to update its contents when the user changes the mouse focus to another cell. The window should now have a close button/icon. It should exist a new option to enable/disable automatic image window appearance. If enabled the window will appear automatically when the mouse is hovering a cell with images. If disabled, the window only appears if the user clicks in the icon that adorns the cell indicating that it has images. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 37" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <div title="Page 36" class="page">
                                  <div class="layoutArea">
                                    <div class="column">
                                      <p>
                                        Difficulty: Medium
                                      </p>
                                      <p>
                                        Mandatory: No
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
</node>
<node CREATED="1460628826880" ID="ID_1391162301" MODIFIED="1460653685136">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Core07- Search
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      An extension that adds support for searching the contents of workbooks.
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
<node CREATED="1460641522975" ID="ID_585236447" MODIFIED="1460647597181" TEXT="Core07.1- Workbook Search">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 38" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000">The extension should provide a new sidebar window for searching the contents of the active workbook. The window should be composed of two parts. The first part (upper part of the window) should contain a textbox for the user to enter a regular expression to be the basis for the search. This part should also contain a button to launch the search. The second part (lower part of the window) should be used to display the search results (cell coordinates and value or contents). The search should include no only the content of the cell (i.e., the text entered by the user) but also its value (that could have been calculated by a formula). </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 37" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <div title="Page 36" class="page">
                                  <div class="layoutArea">
                                    <div class="column">
                                      <p>
                                        Difficulty: Medium
                                      </p>
                                      <p>
                                        Mandatory: No
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
<node CREATED="1460641535483" ID="ID_264260966" MODIFIED="1460647658151" TEXT="Core07.2- Global Search">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 38" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000">The search option should now include all the open workbooks. The list of search results should be interactive. When the user clicks a line in the search result Cleansheets should change the focus to the respective cell. It should be possible to configure the search by selecting the set of type of cells and formulas to include in the search. For instance, the search could only apply to cells of type date. The search should now include also text in cell comments (if selected). </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 37" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <div title="Page 36" class="page">
                                  <div class="layoutArea">
                                    <div class="column">
                                      <p>
                                        Difficulty: Medium
                                      </p>
                                      <p>
                                        Mandatory: No
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
<node CREATED="1460641545117" ID="ID_243317298" MODIFIED="1460647675360" TEXT="Core07.3- Search and Replace">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 39" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="SFRM1000" size="10.000000pt">The extension should now include a new option &quot;Search and Replace&quot;. This new window should be similar to the search window but with an area to enter the replacing text. When search and replace is launched, when a match is found, the window should display &quot;what&quot; and where the match has occurred and how it will become after the replace. The user should then confirm the replacement or select next (to continue the search). The window should include a button to apply the replacing to all the matches without asking each time. Similarly to the search only option, this option should also have parameters to refine the search, for instance, what type of cells to include in the search (or if it should include formulas or comments). </font>
          </p>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 37" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <div title="Page 36" class="page">
                                  <div class="layoutArea">
                                    <div class="column">
                                      <p>
                                        Difficulty: Medium
                                      </p>
                                      <p>
                                        Mandatory: No
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
</node>
<node CREATED="1460628833378" ID="ID_281322538" MODIFIED="1460648012748">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Core08- PDF
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cleansheets should have a new menu option (not an extension) to export to the PDF format. As a suggestion the iText library could be used (http://developers.itextpdf.com).
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
<node CREATED="1460641558513" ID="ID_1242382982" MODIFIED="1460647959978" TEXT="Core08.1- Basic PDF Export">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 39" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">It should be possible to export to PDF an entire workbook, a worksheet or a range of cells. The contents should include only the values of the cells (and not its formulas, for instance). The user should be able to select the content to be exported and also if the document should have a table of contents with links to the sections or not. If select, sections/chapters should be generated for each worksheet of the workbook. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 37" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <div title="Page 36" class="page">
                                  <div class="layoutArea">
                                    <div class="column">
                                      <p>
                                        Difficulty: Medium
                                      </p>
                                      <p>
                                        Mandatory: No
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
<node CREATED="1460641569551" ID="ID_594101652" MODIFIED="1460647989461" TEXT="Core08.2- PDF Style Export">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 39" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">The generated PDF should now mimic as far as possible the screen style of the exported contents. For instance, the formatting of the cells in the PDF should be similar to the screen. It should be also possible to configure the type of lines to use for cell delimitation, the type of line and colour. This is to be applied when rendering all cells in the PDF. Note that this is different from the style used for cells borders in the screen. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 37" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <div title="Page 36" class="page">
                                  <div class="layoutArea">
                                    <div class="column">
                                      <p>
                                        Difficulty: Medium
                                      </p>
                                      <p>
                                        Mandatory: No
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
<node CREATED="1460641579472" ID="ID_1182887253" MODIFIED="1460648009889" TEXT="Core08.3- PDF Complete Export">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 39" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">At this level, the export may include all the contents that are persisted with a workbook. For instance, all the following contents should be exported: the source of formulas, comments, images, macros, etc. However, the user should be given the possibility to select the type of contents to include in the PDF. Therefore, it is expected that the PDF includes sections that represent the visual aspect of the exported worksheets (as far as possible in a similar manner to how they are displayed on screen) and also new sections to include the contents that do not appear in the cells like, for instance, macros, comments or images. In each of this sections the contents should make references to the cells that are related to them (if they are related to cells). </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 37" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <div title="Page 36" class="page">
                                  <div class="layoutArea">
                                    <div class="column">
                                      <p>
                                        Difficulty: Medium
                                      </p>
                                      <p>
                                        Mandatory: No
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
</node>
<node CREATED="1460628837788" ID="ID_666582737" MODIFIED="1460648175866">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Core09- Charts
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      A new extension to add to Cleansheets the possibility of generating Charts.
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
<node CREATED="1460641590257" ID="ID_1895742361" MODIFIED="1460648061388" TEXT="Core09.1- Basic Chart Wizard">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 39" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">Cleansheets should have a new menu option to launch a wizard to help the user create a bar chart. The wizard should have 2 steps. In the first step, the user should input the name of the chart and the range of cells that contains the data for the plot of the graph. The user should also select if the data is in the rows or columns of the range and if the first row or the first column are to be considered labels. In the second step the wizard should display a preview of the graph. The wizard should allow the user to move between steps 1 and 2. If the wizard is confirmed (not cancelled), Cleansheets should display the chart in a new window that should float next to the range of cells used for the chart (and always on &quot;top&quot; of the worksheet). The cell in the left upper corner of the range should have a icon that indicates that the cell has a chart associated with it. Figure 6.1 illustrates an example of a simple bar chart. The chart should be anchored to the worksheet and move with it. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 37" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <div title="Page 36" class="page">
                                  <div class="layoutArea">
                                    <div class="column">
                                      <p>
                                        Difficulty: Medium
                                      </p>
                                      <p>
                                        Mandatory: No
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
<node CREATED="1460641601009" ID="ID_1838386931" MODIFIED="1460648169138" TEXT="Core09.2- Advanced Chart Wizard">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 39" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">The previous bar char can now be configured to display bars side by side (as in Figure 6.1) or stacked (as in Figure 6.2). Cleansheets should also support a new type of charts: pie charts (see example in Figure 6.3). The wizard should now give the user the possibility to select the type of chart. The charts can now be produced using data that is in a different worksheet of the workbook. That is to say that the charts can be visible in worksheets that are different from the worksheets that has the data. Cleansheets should now include a sidebar that displays the list of existing charts and that can be used to enable/disable the display of each chart as well as give access to the chart wizard so that it is possible to change the existing charts. It should also be possible the delete existing charts using this sidebar. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 37" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <div title="Page 36" class="page">
                                  <div class="layoutArea">
                                    <div class="column">
                                      <p>
                                        Difficulty: Medium
                                      </p>
                                      <p>
                                        Mandatory: No
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="prepare"/>
</node>
<node CREATED="1460641611647" ID="ID_178805399" MODIFIED="1460648171970" TEXT="Core09.3- Dynamic Charts">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 40" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="SFRM1000" size="10.000000pt">The charts should now be dynamic. A dynamic chart is a chart that is automatically updated when its source data changes. It should be possible for the user to reposition and resize the charts. Cleansheets should now mark the range of cells that are source to charts with a surrounding border. Also, when the mouse hovers over the left upper corner of a range of cells that is the source of a chart a preview of the chart should appear (even if the &quot;real&quot; chart is located in another worksheet). </font>
          </p>
        </div>
      </div>
    </div>
    <p>
      
    </p>
    <div title="Page 37" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 37" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <div title="Page 37" class="page">
                      <div class="layoutArea">
                        <div class="column">
                          <div title="Page 37" class="page">
                            <div class="layoutArea">
                              <div class="column">
                                <div title="Page 36" class="page">
                                  <div class="layoutArea">
                                    <div class="column">
                                      <p>
                                        Difficulty: Hard
                                      </p>
                                      <p>
                                        Mandatory: No
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="stop"/>
</node>
</node>
</node>
<node CREATED="1460628737838" ID="ID_253554513" MODIFIED="1460644482323" POSITION="left" TEXT="02- Lang">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1460628846913" ID="ID_1805538726" MODIFIED="1460644710919">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lang01- Formulas
    </p>
  </body>
</html></richcontent>
<node CREATED="1460641634289" ID="ID_1780288498" MODIFIED="1460644713560" TEXT="Lang01.1- Block of Instructions">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 41" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 40" class="page">
            <div class="layoutArea">
              <div class="column">
                <p>
                  <font face="SFRM1000" size="10.000000pt">Add the possibility of writing blocks (or sequences) of instructions. A block must be delimited by curly braces and its instructions must be separated by &quot;;&quot;. The instructions of a block are executed sequentially and the block &quot;result&quot; is the result of the last statement of the block. For example, the formula </font><font face="SFTT1000" size="10.000000pt">&quot;= {1+ 2; sum (A1:A10), B3 + 4 }&quot; </font><font face="SFRM1000" size="10.000000pt">must result in the sequential execution of all expressions and the result is the value of the expression </font><font face="SFTT1000" size="10.000000pt">&quot;B3 + 4&quot;</font><font face="SFRM1000" size="10.000000pt">. Add the assign operator (</font><font face="SFTT1000" size="10.000000pt">&quot;: = &quot;</font><font face="SFRM1000" size="10.000000pt">). This operator assigns to its left the result of the right expression. At the moment the left of the assign operator can only be a cell reference. The </font><font face="SFTT1000" size="10.000000pt">FOR </font><font face="SFRM1000" size="10.000000pt">loop should also be implemented based on instruction blocks. For example, the formula </font><font face="SFTT1000" size="10.000000pt">&quot;= FOR {A1: = 1 ; A1&lt;10; A2: = A2 + A1; A1: = A1 + 1 } &quot;</font><font face="SFRM1000" size="10.000000pt">executes a for loop in</font>
                </p>
              </div>
            </div>
          </div>
          <p>
            <font face="SFRM1000" size="10.000000pt">which: the first expression is the initialization, the second term is the boundary condition, all other expressions are performed for each iteration of the loop. </font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="stop-sign"/>
</node>
<node CREATED="1460641646343" ID="ID_1411354188" MODIFIED="1460644240177" TEXT="Lang01.2- Monetary Language">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 41" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">Add a new formulas language (currently Cleansheets only has the Excel formulas that begin with the char- acter &quot;=&quot;). The new language should do only calculations related to currencies. The character that begins the formula should be &quot;#&quot;. The formula should only accept the addition, subtraction, multiplication and division operators. Operands are monetary values in which it is necessary to provide the currency (Ex: </font><font face="SFTT1000" size="10.000000pt">10.21</font><font face="TeX" size="10.000000pt">e</font><font face="SFRM1000" size="10.000000pt">, </font><font face="SFTT1000" size="10.000000pt">1.32&#163; </font><font face="SFRM1000" size="10.000000pt">or </font><font face="SFTT1000" size="10.000000pt">0.20$</font><font face="SFRM1000" size="10.000000pt">). All expressions are required to be contained within brackets with the currency prefix in which we want the result. Ex: </font><font face="SFTT1000" size="10.000000pt">&quot;#euro{10.32$ + 12.89&#163;}&quot; </font><font face="SFRM1000" size="10.000000pt">or </font><font face="SFTT1000" size="10.000000pt">&quot;#dollar{ 10.32$ + 12.89&#163;}&quot; </font><font face="SFRM1000" size="10.000000pt">or </font><font face="SFTT1000" size="10.000000pt">&quot;#pound{10.32$ + 12.89&#163;}&quot;</font><font face="SFRM1000" size="10.000000pt">. For the user to use this language instead of the &quot;regular&quot; Excel language it should start the formula by the character &quot;#&quot; instead of the &quot;=&quot; character). Cleansheets should also provide a way for setting exchange rates (by means of a configuration). The implementation should avoid the use of numbers in floating point representation (e.g., float, double) in order to avoid precision problems. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641658377" ID="ID_1671669576" MODIFIED="1460643353222" TEXT="Lang01.3- Eval and While Loops">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 41" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Add the </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">Eval </span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">function. This function has a single parameter that is a string. When executed, this function will &quot;compile&quot; the formula contained in the only parameter and execute the resulting expression. The result of </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">Eval </span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">is the result of the execution of the compiled expression. For example, if we write the following formula </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">&quot;=&quot; 2 + 3 &quot;&quot; </span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">we get the string </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">&quot;2 + 3&quot; </span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">in the cell. However, if we write the formula </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">&quot;= eval (&quot; 2 + 3 &quot;)&quot; </span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">the value obtained in the cell is </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">5</span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">. Add the following two loop functions: </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">DoWhile </span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">and </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">WhileDo</span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">. The </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">DoWhile </span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">executes the first expression in loop while the second expression evaluates to true. In each iteration of the loop the the first expression is the first to be evaluated. The </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">WhileDo </span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">executes the second expression in loop while the first evaluates to true. In each iteration of the loop the the first expression is the first to be evaluated. Example: </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">&quot;= {@Counter:=1; WhileDo(Eval( &quot;A&quot;&amp;@Counter)&gt; 0; {C1:=C1+Eval(&quot;B&quot;&amp;@Counter); @Counter:=@Counter+1 }) }&quot; </span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">. In this example, the cell C1 will get the sum of all the values of column B in that the corresponding values in column A are greater than zero. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460628856067" ID="ID_593392571" MODIFIED="1460653967111">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lang02- Variables
    </p>
  </body>
</html></richcontent>
<node CREATED="1460641670191" ID="ID_850286159" MODIFIED="1460643345508" TEXT="Lang02.1- Temporary Variables">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 42" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 41" class="page">
            <div class="layoutArea">
              <div class="column">
                <p>
                  <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Add support for temporary variables. The name of temporary variables must start with the &quot;_&quot; sign. When a variable is referred in a formula for the first time it is created. To set the value of a variable it must be used on the left of the assign operator (&quot;:=&quot;). Temporary variables are variables that only exist in the context of the execution of a formula. Therefore, it is possible for several formulas to use tem- porary variables with the same name and they will be different instances. Example: </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">&quot;= {_Counter:=1; WhileDo(Eval( &quot;A&quot;&amp;_Counter)&gt; 0; {C1:=C1+Eval(&quot;B&quot;&amp;_Counter); _Counter:=_Counter+1 }) }&#8221; </span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">. </span></font>
                </p>
              </div>
            </div>
          </div>
          <p>
            <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">In this example, the cell C1 will get the sum of all the values of column B in that the corresponding values in column A are greater than zero. </span></font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641680328" ID="ID_1171410507" MODIFIED="1460643329411" TEXT="Lang02.2- Global Variables">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 42" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Add support for global variables. Global variables are variables that exist in the context of the workbook and are persisted with the workbook. The name of global variables must start with the &quot;@&quot; sign. When a variable is referred in a formula for the first time it is created. To set the value of a variable it must be used on the left of the assign operator (&quot;:=&quot;). Attention should be given to synchronization problems. For an example of the use of global variables see </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">Lang01.3</span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641691030" ID="ID_572362915" MODIFIED="1460643322107" TEXT="Lang02.3- Arrays and Variable Editor">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 42" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Add support for array variables (temporary and global). Any variable can be an array. When accessing a variable only by its name it should be inferred the position 1 of the array. To explicitly access a position in a array variable the position index should be specified inside brackets (following the name of the variable). For example, the formula </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">&quot;=@abc[2]:=123&quot; </span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">will set the position 2 of the global variable @abc to the value 123. Each position of an array can be of a different type. For instance it should be possible to have an array with numeric and alphanumeric values. </span></font>
              </p>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">There should also be a window in the sidebar to display and edit the value of global variables. The values that appear in this window should be automatically updated when the variables are updated. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460628864852" ID="ID_1829156480" MODIFIED="1460653968715">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lang03- Tools Based on Formulas
    </p>
  </body>
</html></richcontent>
<node CREATED="1460641704573" ID="ID_1000015472" MODIFIED="1460643314328" TEXT="Lang03.1- Conditional Formatting of Cells">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 42" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Update the &quot;style&quot; extension so that it can be used for the conditional formatting of cells based on the result of the execution of formulas. For the style of a cell to be conditional it must have an associated formula and two formatting styles. One of the styles is applied when the formula evaluates to true and the other when it evaluates to false. The editing of these settings should be done in a sidebar window. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641716881" ID="ID_1520512872" MODIFIED="1460644293689" TEXT="Lang03.2- Conditional Formatting of Ranges">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 42" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">Enable Cleansheets to apply conditional formatting to a range of cells (also in the style extension). The idea is that a single formula could applied to all the cells in the range (one at a time) in order to evaluate what style to apply. For that to be possible it is necessary to add a new special kind of variable to the formulas that represents the &quot;current&quot; cell. This special variable could be named &quot;_cell&quot;. For instance, the formula </font><font face="SFTT1000" size="10.000000pt">&quot;=_cell &gt;= 10&quot; </font><font face="SFRM1000" size="10.000000pt">could be associated to a range format. In this case, Cleansheets would evaluate the formula for each cell in the range and apply the formatting style in accordance with the result of the formula. In this example, all cell in the range with a value greater or equal to 10 would receive the style associated with the true result and the others the style associated with the false result. The window in the sidebar should also be updated so that it is clear if the format is for a single cell or for a range. Within the sidebar window it should also be possible to remove existing conditional style formatting. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641728676" ID="ID_842568036" MODIFIED="1460643296664" TEXT="Lang03.3- Tables and Filters">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 42" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Add a new extension to support the concept of &quot;tables&quot;. A table is essentially a range of cells. The first row of this range of cells can be used as header of the table columns (the contents of these cells become the name of the columns). Once a table is defined it should be possible to filter its contents by using formulas. A formula that is used as a filter of a table is applied to each row of the table. If the result is true, the row is visible, if the result is false, the row should become invisible. To facilitate the writing of such formulas a new special variable should be added to formulas. This new variable should be an array variable that represents the value of the columns of the table for the current row. Lets consider, for instance, that the new variable is called </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">&quot;_col&quot;</span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">. For example, it should be possible to use </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">&quot;_col[2]&quot; </span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">to get the value of column 2 for the current row. It should also be possible to use the name of the column instead of the index. For instance, if the header of column 2 is &quot;cidade&quot; it should be possible the get the value of this column for the current row by using </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">&quot;_col[&quot;cidade&quot;]&quot;</span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">. An example of a filter for a table could be: </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">&quot;=or(_col[&quot;idade&quot;]&gt;10; _col[3]&lt;123)&quot;</span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">. This extension should add a new sidebar window that should be used to edit tables and its filters. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460628885154" ID="ID_1586770063" MODIFIED="1460653969527">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lang04- Function Wizard
    </p>
  </body>
</html></richcontent>
<node CREATED="1460641740335" ID="ID_445211232" MODIFIED="1460643287973" TEXT="Lang04.1- Insert Function Basic Wizard">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 43" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Cleansheets should have a menu option to launch a wizard to aid the user in calling functions in formulas. This new window should display a list of possible functions. The construction of this list should be made dynamically based on the properties file and self-description of the functions. When a function is selected in the list its syntax should be displayed in a edit box. The &quot;syntax&quot; should include the name of the functions and its parameters. For example, for the factorial function, that only has one parameter, the following text should be displayed in the edit box </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">&quot;= FACT(Number)&quot;</span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">. The window should also contain an area to display a text describing the selected function (i.e., help text). The window should have an &quot;Apply&quot; and a &quot;Cancel&quot; button. If the user selects the &quot;Apply&quot; button the text of the syntax of the function should be written in the formula bar. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641752535" ID="ID_1673645406" MODIFIED="1460643281171" TEXT="Lang04.2- Insert Function Intermediate Wizard">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 43" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">The wizard window should display an edit box for each parameter of the selected function. The user should use these edit boxes to enter the values for each parameter of the function. As the user enters the values the wizard should display (in a new region of the window) the result of the execution of the formula or a message explaining the problem. The function list should now include also the operators as well as the functions that are dynamically loaded from </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">java.lang.Math</span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">. The wizard should be now launched from an icon or button located in the formula bar, between the label of the active cell and the edit box of the formula/value of the current cell. The menu option should be removed. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641768969" ID="ID_456238972" MODIFIED="1460643273969" TEXT="Lang04.3- Insert Formula Advanced Wizard">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 43" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">The wizard should now have an edit box where the formula is gradually constructed. The user should be able to edit the formula text freely. The functions or operators (and the values of its parameters/operands) selected from the list should now be inserted in the position of the cursor in the new editbox. The wizard should continue to have an area to display the evaluation of the formula (that should be produced dynamically, as the user edits the formula). The wizard should also have a new window that should display the structure of the formula expression like an abstract syntax tree (i.e., the structure resulting from the formula compilation). When the user clicks a tree element its respective text in the edit box should appear highlighted. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460628893801" ID="ID_1445099691" MODIFIED="1460653970124">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lang05- Macros
    </p>
  </body>
</html></richcontent>
<node CREATED="1460641786312" ID="ID_1985120824" MODIFIED="1460643265173" TEXT="Lang05.1- Macro Window">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 43" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">The new extension should add a menu option to open a window to edit and execute a single macro. Macros should be designed as a complete new language in Cleansheets. However, its initial grammar should be very simple and based on the formulas of Cleansheets. In particular, a macro is simply a sequence of formulas that are executed sequentially. The formulas are the same as those used in the cells. Each line of the macro may contain a formula or be a comment. A comment is a line that starts with the character &quot;;&quot;. The lines of the macros must support all that is possible to do with the cell formulas that start with &quot;=&quot; (but in the macros the lines do not need to start the line with &quot;=&quot;). The macro is to be associated with the current workbook (but, for the moment, it is not required to persist the macro). The result of executing a macro is the result of the last executed instruction. The new window should have an area to edit the text of the macro and button to run the macro. The result of the execution of the macro should also appear in the window. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641796871" ID="ID_771360606" MODIFIED="1460644365169" TEXT="Lang05.2- Multiple Macros">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 43" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">Cleansheets should now support multiple macros. Each macro should have a name and should be associated with an workbook (and also persist with the workbook). The grammar of the macros should also have a mechanism to support the invocation of macros. It only should be possible to invoke macros of the same workbook. Special attention should be devoted to recursion (i.e., avoiding infinite recursion). </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641806453" ID="ID_703442487" MODIFIED="1460643249417" TEXT="Lang05.3- Macros with Parameters">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 44" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Macros should now have parameters. The syntax for macros should now include an header that should include the name of the macro and its parameters (all parameters should have a distinct name). The parameters should be considered only input parameters. However, it should be possible to freely reference parameters inside the macro. That is to say that, inside a macro, parameters should be used like variables. Macros should support local variables that exist only in the context of a macro. This local variables should have a syntax similar to the one described in Lang02.1 for the temporary variables of formulas. The invocation of macros must now include the values for its parameters. </span></font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460628899421" ID="ID_583041710" MODIFIED="1460653970592">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lang06- Forms
    </p>
  </body>
</html></richcontent>
<node CREATED="1460641818190" ID="ID_1293146111" MODIFIED="1460643241676" TEXT="Lang06.1- Forms Editor">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 44" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Cleansheets should have a new menu option to launch a window for editing a form. A Form is a window that is designed by the end user and is used for interacting with the user (input and output). The new window should support the creation and testing of a Form. Forms should be very simple. A Form should be composed of rows, each row can be empty or have one or two visual widgets. The supported visual widgets are: button (to invoke actions); edit box (to enter data) and static text box (to display data). It should be possible to set the core properties of these widgets (like the text to display in a static text box, for instance). In the edit form window it should be possible to: add a new row; remove an existing row; edit an existing row; &quot;play&quot; the form and close the edit form window. The &quot;play&quot; button is for testing the appearance of a form during its design (see example in Figure 6.4). At the moment it is only required to support a single Form for each workbook. It is not required to persist the Form. Macros and formulas should have a new function that can be used to display the form of the current workbook. Forms should have an icon or button to close de form. When the form is closed the function (in macros or formulas) who call it returns. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641828161" ID="ID_880935086" MODIFIED="1460643234682" TEXT="Lang06.2- Forms and Variables">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 44" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">In order for forms to become useful in formulas or macros it is necessary to associate data with the contents of the visual widgets. The mechanism used for that will be the binding of variables (macros or formulas variables) with the contents of the visual widgets. One simple way to achieve this is by using temporary variables (from macros and formulas). The matching between widgets and variables should be done by associating the ones with the same name. When displaying a form (in the context of a macro or a formula), if the temporary variables with the same name of widgets exist, them they are used to set the content of the widgets. For widgets for which no temporary variables with the same name are found then new temporary variables should be created. The user should be able to change the contents of edit boxes. When closing the form window the contents of the temporary variables should be updated from the contents of the corresponding visual widgets. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641838712" ID="ID_358618626" MODIFIED="1460643227015" TEXT="Lang06.3- Advanced Forms">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 45" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 44" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <p>
                      <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should now be possible to create multiple forms for each workbook. Forms should also be persistent with the workbook. To distinguish Forms each one should have a unique name (within its workbook). The function (in macros and formulas) that displays the forms should now have one parameter that is used to pass the name of the form to display (since there can be several forms for each workbook). </span></font>
                    </p>
                    <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">When displaying a form it should be possible specify if it should be readonly (i.e., it will display the value of the variables but does not allow any update) or writable (in this case the form should allow for the user to modify the values that are displayed). Writable forms should have a new &quot;Update&quot; button. When the user clicks in the update button the form closes and the current values of the widgets update the corresponding variables. If the user closes the form window by any other means the variables should not be updated. It should be also possible to specify the &quot;mode&quot; of the form window (when invoking the display of a form). Two modes are allowed: modal and modeless. A modal form window is a window that will block the macro or formula that call it until the user closes the form window. The macro or formula will only resume execution when the form is closed. A modeless for window is a window that will no block the calling macro or formula, i.e., the macro or formula will continue its execution in parallel with the display of the form. Modeless forms do not return anything. Model forms should return the name of the button that was used to close the window. </span></font>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460628905366" ID="ID_299105978" MODIFIED="1460653971219">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lang07- Java Scripting
    </p>
  </body>
</html></richcontent>
<node CREATED="1460641849630" ID="ID_1400522503" MODIFIED="1460643205309" TEXT="Lang07.1- BeanShell Window">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 45" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Cleansheets should have an extension to support scripts in the BeanShell scripting language. Macros and BeanShell should be in the same extension. The extension should add a menu option that will launch a window to edit and execute a BeanShell script. BeanShell and Macros should share the same editing window (please see Lang05). The idea is that the user will be able to select (in this window) the type of language he wants to use to customize Cleansheets. </span></font>
              </p>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">In this iteration, and to test BeanShell support, if the user selects the BeanShell language, the edit window should automatically be filled with a script that: (1) opens a new workbook, (2) creates a macro, (3) run the created macro and (4) displays a window with the result of the invocation of the macro. Similarly to macros, the result of a BeanShell script is the last value (result of the last instruction). The result of executing the BeanShell script should be displayed in the same area of the edit window that also displays the results of the execution of macros. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641860026" ID="ID_561112430" MODIFIED="1460643196913" TEXT="Lang07.2- BeanShell Integration">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 45" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should now be possible to invoke Beanshell scripts from macros and formulas using a new function. The new function should be able to execute Beanshell scripts synchronously or asynchronously. If the execution mode is synchronous, then the function should wait for the script to end its execution. In this case the return value of the function should be the value of the last expression of the Beanshell script. If the execution mode is asynchronous then the function should return immediately after launching the execution of the script: the script and the formula/macro will execute in parallel. Existing variables in the macro or formula that executes the script should be accessible inside the Beanshell script. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641872078" ID="ID_1399804940" MODIFIED="1460643190709" TEXT="Lang07.3- Cleansheats API">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 45" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Design and implement a mini API of the Cleansheets objects inside Beanshell. The new API should provide access to objects like workbook, worksheet, cells, macros, variables, etc. This API should prevent as far as possible access to &quot;internal&quot; classes of Cleansheets. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460628912275" ID="ID_653065072" MODIFIED="1460653973477">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lang08- XML
    </p>
  </body>
</html></richcontent>
<node CREATED="1460641882110" ID="ID_1997812135" MODIFIED="1460643184340" TEXT="Lang08.1- Export XML">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 45" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to export the contents of an workbook, worksheet or part of an worksheet to an XML file. As we want to optimize as much as possible the process the solution should not rely on any third party library. Cleansheets should have a window to configure the XML tags to use for each type of element. The export should only include the value of the cells. The export menu option should appear in the File menu. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641893285" ID="ID_360095408" MODIFIED="1460643177863" TEXT="Lang08.2- Import XML">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 45" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to import data from an XML file (this operation is the &quot;inverse&quot; of the previous FI). Depending on the contents of the XML file, the data from the file can replace the contents of a workbook, a worksheet or a range of a worksheet. This option should appear in the File menu. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641902824" ID="ID_1814504947" MODIFIED="1460643170886" TEXT="Lang08.3- Full XML Import/Export">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 46" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">The previous options should now include all the persisting elements of Cleansheets (i.e., all the contents that are normally persisted with the workbook, such as macros, formatting styles, comments, etc.). The import should now ask the user if the file contents should replace or append the existing workbook contents. </span></font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1460628746617" FOLDED="true" ID="ID_171019385" MODIFIED="1460653962826" POSITION="right" TEXT="03- IPC">
<node CREATED="1460628919540" ID="ID_1924597080" MODIFIED="1460629494183">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      IPC01- Cells Sharing
    </p>
  </body>
</html></richcontent>
<node CREATED="1460641932994" ID="ID_1418329750" MODIFIED="1460644734932" TEXT="IPC01.1- Start Sharing">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 46" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to establish a connection with other instance of Cleansheets in the local network. It should be possible to send the contents of a range of cells to another instance of Cleansheets. The other instance should display the received contents in the same cell address as the original cells. </span></font>
              </p>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to configure the port to be used for network connections. It should be possible to find other instances of Cleansheets available in e local network. These instances should be listed in a new window (sidebar window). The user should be able to select one of the discovered instances to connect to when establishing the connection to send the contents of the range of cells. At the moment it is only required to send the value of the cells. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="stop-sign"/>
</node>
<node CREATED="1460641945338" ID="ID_498052868" MODIFIED="1460643123764" TEXT="IPC01.2- Sharing&#x2019;s Automatic Update">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 46" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Once a connection is stablished between two instances of Cleansheets updates made in one side must be automatically sent to the other side. The data shared must include now also the style of the cells. At the moment It is not necessary to support the sharing of cells with formulas. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641956848" ID="ID_1598503916" MODIFIED="1460643117187" TEXT="IPC01.3- Multiple Sharing">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 46" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to have multiple cell shares active at the same time. Each of the shares should have a unique name. The location (i.e., range address) of the share in each instance of Cleansheets may be different. It should be possible to share ranges that include cells with formulas. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460628928247" ID="ID_625789240" MODIFIED="1460629503188">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      IPC02- Search Local Workbooks
    </p>
  </body>
</html></richcontent>
<node CREATED="1460641968212" ID="ID_1401501394" MODIFIED="1460643110319" TEXT="IPC02.1- Find Workbooks">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 46" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">The extension should add a new window (sidebar) to search for workbook files in the local file system. The user should be able to enter the name of a directory of the file system to be used as the root of the search. The search should include this directory and all its contents (including subdirectories). The results of the search should appear in a list (as files are found). It should be possible for the user to open a workbook from this list by double clicking in it. The search can be based solely on the file name extension. For instance, find the files with </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">.cls </span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">extension. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641977574" ID="ID_358957043" MODIFIED="1460643103141" TEXT="IPC02.2- Advanced Workbook Search">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 46" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">The sidebar window that displays the results of the search should now include an area to display a preview of the contents of a workbook when the user selects it (i.e., clicks on it). The preview should be based on the values of the first non-empty cells of the workbook. This preview should be produced without open the worksheet (at least without the worksheet been opened in the user interface). The search should now be based on a pattern and not only on the file name extension. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460641988646" ID="ID_450182749" MODIFIED="1460643096765" TEXT="IPC02.3- Multiple Realtime Workbook Search">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 46" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should now be possible to have several search windows. Each search window should have an option, for instance a check button, to indicate if the search is an active search. An active search will be a search that keeps updating the contents of the list that displays the results. The preview for each file should now be cached so that Cleansheets only produces the preview the first time the user request it or when the contents of the file change. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460628938907" ID="ID_398933069" MODIFIED="1460629511017">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      IPC03- Distributed Workbook Search
    </p>
  </body>
</html></richcontent>
<node CREATED="1460642001498" ID="ID_363853484" MODIFIED="1460643088044" TEXT="IPC03.1- Search in Another Instance">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 47" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to send a request for searching workbooks to another instance of Cleansheets. The search should be based on the name of the workbook (a pattern of the name). The search should only include workbooks that are open in the remote instance of Cleansheets. The reply should must inform if the workbook was find or not. If the workbook was find then the reply must also include a summary of the contents of the workbook. This summary should include the name of the worksheets and the values of the first non-empty cells of each worksheet. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642012997" ID="ID_1945042379" MODIFIED="1460643080342" TEXT="IPC03.2- Search in the Network">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 47" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to broadcast a workbook search request to all the instances of Cleansheets in the same local network. The search should only include the workbooks that are open. Cleansheets should have a sidebar window to display - in a list - the results of the search. This window should be updated as replies as received. The list of results should include the identification of the instance where the workbook was found, the name of the workbook and a summary of the contents of the workbook. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642024642" ID="ID_1859103813" MODIFIED="1460643073713" TEXT="IPC03.3- Network Search by File Contents">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 47" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">The network search should now be based on a pattern for the name of the workbook (as before) or a pattern of the contents of the workbook (or both). The search should also include not only the open workbooks but also the workbooks that exist in the remote file systems. The window with the list of results should be updated automatically as the replies are received. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460628955758" ID="ID_1452341632" MODIFIED="1460642070186">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      IPC04- Import/Export Data
    </p>
  </body>
</html></richcontent>
<node CREATED="1460642038495" ID="ID_273745983" MODIFIED="1460643065754" TEXT="IPC04.1- Import/Export Text">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 47" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to export and import data to/from a text file. Each line in the text file represents a row of data. In each line a special character is used to separate columns. The user should be able to configure this character and also define if the first line of the text file should be treated as containing the header of the columns or as regular row. The user should also enter a range of cells to be used as source (export) or destination (import) for the operation. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642048573" ID="ID_72527483" MODIFIED="1460643054629" TEXT="IPC04.2- Import/Export Text Link">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 47" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">The process of creating a link is simular to the one described in IPC04.2 but the import or export should be always active (until it is removed by the user). Being active means that the process will be repeated automatically when the source of the data is updated. This should happen for imports and exports. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642059083" ID="ID_175239192" MODIFIED="1460643997382" TEXT="IPC04.3- Import/Export Database">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 47" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">It should be possible to export and import data to/from a table in a database. Each row in the table corresponds to a row in Cleansheets and each column in the table corresponds to a column in Cleansheets. The user should enter a range of cells to be used as source (export) or destination (import) for the operation. The first row of the range should be treated as a header. Each column in the first row is used as the name of the corresponding database column. This feature should be based in jdbc (Java Database Connectivity). The user should specify a database connection to be used and the name of the table. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460628963541" ID="ID_1043103984" MODIFIED="1460629525534">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      IPC05- Chat Application
    </p>
  </body>
</html></richcontent>
<node CREATED="1460642077391" ID="ID_230437019" MODIFIED="1460644028697" TEXT="IPC05.1- Chat Send Message">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 47" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="SFRM1000" size="10.000000pt">Add a option that can be used to send text messages to another instance of Cleansheets. The message should be displayed in a popup window in the other instance of Cleansheets. The popup should disappear after some short period (for instance 5 seconds). Cleansheets should have have a new sidebar window to display all the messages. The sidebar should be based on a tree control that shows the messages grouped by thread of conversation (i.e., each reply will be a child of the parent message). It should be possible to reply to a message by double clicking on it in the tree. </font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642087299" ID="ID_40113725" MODIFIED="1460643032221" TEXT="IPC05.2- Chat Participants">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 48" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Cleansheets should now use the user name of the system as the basis for the user profile of the chat. The end user should be able to add an icon or a photo to its profile as well as a nickname. Each user should have have a status (i.e., online or offline). Cleansheets should automatically discover all users in the local network. The sidebar window should now have the conversations organized by user. The window should also display the status of the users and their icon and nickname. When a user state is offline it will not receive any messages from other instances of Cleansheets. Profile configuration and message history should be persistent. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642096822" ID="ID_503706109" MODIFIED="1460644041267" TEXT="IPC05.3- Chat Rooms">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 48" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">The chat extension should now support the concept of chat room. A chat room can have several participants. Messages in a chat room are broadcasted to all its members. The user that creates a chat room becomes its owner. There are 2 types of rooms: private rooms and public rooms. Public rooms are announced to all instances of Cleansheets and each user is free to become a member of a public room. A private room is not announced in the network. The owner should send invites to other users to participate on the room. Each user is free to accept or reject the invitation. The sidebar should now display also the chat rooms. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460628970681" ID="ID_906912628" MODIFIED="1460629532420">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      IPC06- Networking Tools
    </p>
  </body>
</html></richcontent>
<node CREATED="1460642106956" ID="ID_874547632" MODIFIED="1460643016290" TEXT="IPC06.1- Secure Communication">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 48" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">There should be a new mechanism to add secure communications (encrypted communications) between instances of Cleansheets. It is not required at this moment that the cypher should be &quot;professional&quot;, only that it should not be trivial to break it. It should be possible to establish secure and unsecure communications with other instances. Cleansheets should now have a new window that logs all the incoming and outgoing communications. Therefore, when this window is activated it should be possible to see encrypted and unsecure data being exchanged. For testing purposes it should be possible for the user to send simple text messages either unsecure or encrypted. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642118313" ID="ID_1157389550" MODIFIED="1460644139316" TEXT="IPC06.2- Network Analizer">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 48" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt">Cleansheets should now have a new sidebar window that displays a real time chart of all incoming and outgoing network traffic. There should be four columns: unsecure incoming; secure incoming; unsecure outgoing; secure outgoing. The chart should automatically adjust the units used: bytes; kilobytes; megabytes and gigabytes. The unit used should be the one that results in a chart that is more adjusted to the size of the window. </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642127004" ID="ID_1930340443" MODIFIED="1460643000695" TEXT="IPC06.3- Network Explorer">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 48" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Cleansheets should now have a new sidebar window to display information about all the instances of Cleansheets in the same local network. The window should be based on a tree control. The tree should display at the root all instances of Cleansheets. For each instance it should display its resources as branches of the root element corresponding to the instance. The information should include: workbooks and respective worksheets; loaded extensions (if they are active or inactive) and its description and version. This tree should be updated in real time. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460628978541" ID="ID_280607959" MODIFIED="1460629540499">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      IPC07- Networking Games
    </p>
  </body>
</html></richcontent>
<node CREATED="1460642132690" ID="ID_1184515864" MODIFIED="1460642993141" TEXT="IPC07.1- Choose Game and Partner">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 48" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">With this extension it should be possible for two instances of Cleansheets to establish a connection to play games. Cleansheets should display a list of all instances of Cleansheets in the local network. It should be possible to invite another instance to play one of the following games: tic-tac-toe (</span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">https://en.wikipedia. org/wiki/Tic-tac-toe</span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">) or battleships (</span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">https://en.wikipedia.org/wiki/Battleship_(game)</span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">). Each instance should be able to define the profile of the user. The user name should be the user name of the system. The user should be able to configure an icon or photograph to be associated to his profile. The name of the users and icon should be displayed in the list of available users to play with. The user should be able to play several games at the same time. There should be a sidebar to display all the active games and also all the online users (i.e., instances of cleansheets). It should be possible to end a game. At this moment it is not required to effectively implement the games. </span></font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642167377" ID="ID_1052570786" MODIFIED="1460642985028" TEXT="IPC07.2- Tic-tac-toe">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 49" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">The tic-tac-toe game should be implemented. The game should be implemented as described in </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">https: //en.wikipedia.org/wiki/Tic-tac-toe</span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">. The board of the game should be displayed in a range of cells. Each cell should correspond to a position in the game board. Moves are performed by placing an &quot;X&quot; or an &quot;O&quot; in the cells. The user should only be able to perform a move when its his turn. Boards are updated on both instances. Cleansheets should automatically verify and enforce the rules of the game. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642175957" ID="ID_1371965544" MODIFIED="1460642978587" TEXT="IPC07.3- Battleships">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 49" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">The batleships game should be implemented. The game should be implemented as described in </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">https: //en.wikipedia.org/wiki/Battleship_(game)</span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">. The board of the game should be displayed in a range of cells. Each cell should correspond to a position in the game board. Moves are performed by sending cell addresses of the board to the other instance of Cleansheets. The user should only be able to perform a move when its his turn. Cleansheets should automatically verify and enforce the rules of the game. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460628990234" ID="ID_1768841432" MODIFIED="1460629546869">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      IPC08- File Sharing
    </p>
  </body>
</html></richcontent>
<node CREATED="1460642186750" ID="ID_357991504" MODIFIED="1460644205413" TEXT="IPC08.1- File Sharing">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 49" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000">Cleansheets should have a new option to share the files contained in a specific directory. The user should be able to specify the directory to share (output). These files should now be listed on other instances of Cleansheets (in a specific window, for instance, in a sidebar). The list should include the name of the files and its size. It is also required to configure the local directory that will receive the downloaded files (input). The configuration of file sharing should be persistent. For the moment it is not required to implement the download of files, however it is necessary to keep the list of files updated automatically. It is also necessary to update the list of files that where selected for download in the input list. This list should include the name of the file, its size, its source and its status (download in progress, up to date, etc.). </font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642194628" ID="ID_1771357872" MODIFIED="1460642962118" TEXT="IPC08.2- Automatic Download">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 49" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">The download of a file should be executed without blocking Cleansheets. The progress of the download should be displayed in a window. When the user selects a file for download it must specify if it will be a one time download or a &quot;permanent&quot; download. A permanent download is one that will download the file each time the file is updated in the source. For a permanent download the user must specify if each new download should replace the previous version of the file or not. If not, each download of the file should create a new file with a name that as a suffix the number of the version. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642203826" ID="ID_1136903467" MODIFIED="1460642954709" TEXT="IPC8.3- Peer Download">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 49" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">This is an optimization of the download. Cleansheets should be able to identify that the same file exists in several instances of Cleansheets. With this information it should be able to select the more convenient source for the download of a file. For instance, if a source of a file download becomes unavailable then Cleansheets should automatically select another source and continue the download. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1460628757277" FOLDED="true" ID="ID_449431554" MODIFIED="1460653961317" POSITION="left" TEXT="04- CRM">
<node CREATED="1460628999704" ID="ID_1863779645" MODIFIED="1460629559860">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CRM01- Contacts
    </p>
  </body>
</html></richcontent>
<node CREATED="1460642220448" ID="ID_955840757" MODIFIED="1460644725616" TEXT="CRM01.1- Contact Edition">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 50" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 49" class="page">
            <div class="layoutArea">
              <div class="column">
                <p>
                  <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">A sidebar window that provides functionalities for creating, editing and removing contacts. Each contact should have a first and last name and also a photograph. Each contact should also have one agenda in which events related to the contact should be displayed. For the moment, events have only a due date (i.e., timestamp) and a textual description. It should be possible to create, edit and remove events. The agenda may be displayed in a different sidebar. This sidebar should display a list of all events: past, present and future. One of the contacts should be the user of the session in the computer where Cleansheets is running. If this user has events then, when their due date arrives, Cleansheets should display a popup</span><span style="font-family: SFRM1000; font-size: 10.000000pt">&#160;</span></font>
                </p>
              </div>
            </div>
          </div>
          <p>
            <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">window notifying the user about the events. This popup window should automatically disappear after a small time interval (e.g., 5 seconds). </span></font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<icon BUILTIN="stop-sign"/>
</node>
<node CREATED="1460642231138" ID="ID_1551314225" MODIFIED="1460642885311" TEXT="CRM01.2- Company Contact">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 50" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">A contact may also be a company. If a contact is a company then it has a name (no first and no last name). A person contact may now be related to a company contact. A person contact may have also a profession. The profession should be selected from a list. The list of professions should be loaded (and/or updated) from a external xml file or an existing configuration file of Cleansheets. The window for company contacts should display all the person contacts that are related to it. The company window should also have an agenda. The agenda of a company should be read only and display all the events of the individual contacts that are related to it. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642240882" ID="ID_214283899" MODIFIED="1460642876637" TEXT="CRM01.3- Contacts with Tags">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 50" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to associated tags to contacts (individual or company contacts). A tag is simple an alphanumeric word. Cleansheets should have a window to search contacts based on tags. The search should be based on regular expressions related to tags. Cleansheets should display the search results in a list. The user may click on an item of the list to edit the corresponding contact. There should also be a window with a list of tags that is automatically sorted (descending) based on the frequency of the tag utilization. The list should display in each line the tag and its frequency. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460629007584" ID="ID_490304038" MODIFIED="1460629566565">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CRM02- Addresses
    </p>
  </body>
</html></richcontent>
<node CREATED="1460642250804" ID="ID_453575884" MODIFIED="1460642867882" TEXT="CRM02.1- Address Edition">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 50" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">There should be a new sidebar window to create, edit and remove addresses associated with contacts (individuals or companies). Each address must include: street, town, postal code, city and country. Each contact should have to addresses: the main address and a secondary address. In the case of a Portuguese address the postal code should be validated. It should be possible to import and update the the list of valid Portuguese postal codes from an external file (xml file or other format). </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642259896" ID="ID_72505841" MODIFIED="1460642864122" TEXT="CRM02.2- Contact Import/Export">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 50" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to import and export contacts and addresses from/to an worksheet. It should be possible to specify the fields to include in the process. The user should be able to enter an expression to select the contacts to be exported. The expression should function like a filter involving relational operators (i.e., &gt;, &gt;=, &lt;, &lt;=, =) as well as logical operators (i.e., AND, OR, NOT). The user should define if the operation is a one-time operation or an active operation. If it is an active operation them the operation must be repeated (automatically) each time its source is also updated. Cleansheets should have a window to list all active import and export operations and should allow the user to edit or remove these operations. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642269051" ID="ID_1300882405" MODIFIED="1460642850371" TEXT="CRM02.3- PDF Envelopes">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 50" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to generate envelopes in PDF format for a set of contacts. The contacts may be selected based on any of their fields or addresses. It should be generated one PDF envelope for each of the selected contacts. All the envelopes should have the same sender. By default the sender should the the contact associated with the logged user. The user should select if the address used for the recipients is the main or the secondary address. The PDF files should be produced in a directory selected by the user. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460629019778" ID="ID_1264624834" MODIFIED="1460629573708">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CRM03- Email and SMS
    </p>
  </body>
</html></richcontent>
<node CREATED="1460642278434" ID="ID_384448007" MODIFIED="1460642841576" TEXT="CRM03.1- Email and Phone Edition">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 50" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">There should be a window to create, edit and remove telephone numbers and emails of contacts. Clean- sheets should only accept syntactically correct emails and telephone numbers. Each company can have a main email and telephone number and several secondary emails and telephone numbers. Each individual contact can have the following types of telephone numbers: work; home; celular1 and celular2. Each individual contact can have a main email and several secondary emails. Cleansheets should validate the syntax of the telephone numbers based on the country code. The list o countries and its phone prefixes should be imported and updated from an external file. It should also validate the format o emails. </span></font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642289590" ID="ID_1147706235" MODIFIED="1460642830139" TEXT="CRM03.2- Import/Export Emails and Phone Numbers">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 51" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to import and export the phone and email contacts from/to a worksheet. The user should be able to enter an expression to select the contacts to be exported. The expression should function like a filter involving relational operators (i.e., &gt;, &gt;=, &lt;, &lt;=, =) as well as logical operators (i.e., AND, OR, NOT). The expression should involve only fields related to the contacts. The user should define if the operation is a one-time operation or an active operation. If it is an active operation them the operation must be repeated (automatically) each time its source is also updated. Cleansheets should have a window to list all active import and export operations and should allow the user to edit or remove these operations. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642301924" ID="ID_1108163810" MODIFIED="1460642819874" TEXT="CRM03.3- Send Email/SMS">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 51" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to send emails and text messages (i.e., SMS). It should be possible to select the recipient of the message based on address, profession or tag. The functionality that sends emails must be implemented. For SMS it is only required to simulate the operation. There should be window that displays a list of emails and text messages that where sent. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460629030442" ID="ID_1290009087" MODIFIED="1460629581919">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CRM04- Notes and Lists
    </p>
  </body>
</html></richcontent>
<node CREATED="1460642311505" ID="ID_901587429" MODIFIED="1460642798689" TEXT="CRM04.1- Notes Edition">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 51" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to create, edit and remove text notes associated with contacts. Each contact can have one or more notes. There should be a sidebar to list the textual notes of a contact. A text note should be entered as multiline text in which the first line is interpreted as the title of the text note. The timestamp of the creation of the note should be also associated with the text note. Cleansheets should maintain the history of modifications for each text note. When a text note is selected Cleansheets should display its history. For each version, Cleansheets should display the timestamp and the first line (i.e., the title). </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642320252" ID="ID_889518299" MODIFIED="1460642789409" TEXT="CRM04.2- Lists Edition">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 51" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to create, edit and remove list notes. A list note is similar to a textual note but each line is displayed as a check box (that can be checked or unchecked). The first line is also interpreted as the title of the list note. </span></font>
              </p>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to generate a new version of a text note or list based on a old version of it. When this happens Cleansheets should &quot;open&quot; the new version for edit with the same contents of the old version. This is the only &quot;trace&quot; that may eventually link to the old version. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642330206" ID="ID_1028380641" MODIFIED="1460642780348" TEXT="CRM04.3- Search and export Notes">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 51" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to search for notes (text and lists) within an time interval. The query expression should also allow to search based on the contents of the notes. It should be possible to user regular expressions to search the contents of the notes that are within the time interval. The result should list all the notes that where found. It should be possible to open a specific note by double clicking on it in the result list. It should be possible to export the search results to a range in an worksheet. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460629036385" ID="ID_1192322342" MODIFIED="1460629593207">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CRM05- Calendar
    </p>
  </body>
</html></richcontent>
<node CREATED="1460642343452" ID="ID_28066498" MODIFIED="1460642771269" TEXT="CRM05.1- Calendar Edition">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 51" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Cleansheets should have a new window to create, edit and remove calendars. Each contact can have one or more calendars. Each calendar has a name (e.g., work, birthdays, home, etc.) and a textual description. It should be possible to associate a colour to a calendar. The agenda of contacts must be upgraded to support the calendars of the contact. The events of the contact should now be associated with a calendar. Events should also be upgraded to include a duration. The agenda of a contact should now display events from all its calendars. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642354053" ID="ID_290294220" MODIFIED="1460642759281" TEXT="CRM05.2- Basic Agenda Window">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 52" class="page">
      <div class="layoutArea">
        <div class="column">
          <div title="Page 51" class="page">
            <div class="layoutArea">
              <div class="column">
                <ol style="list-style-type: none" start="0">
                  <li>
                    <p>
                      <font face="SFRM1000" size="10.000000pt">Cleansheets should now have a &quot;special&quot; window to display the agenda of a contact. It should be possible to select the contact from a dropbox. The window should display the agenda of the selected contact. The window should also have a field to enter a date in the format DD-MM-YYYY. The window should display the agenda of the selected contact for the selected day. The window should display the day of the week for the given day. The window should also have two buttons: one to move to the next day and the other</font>
                    </p>
                    <font face="SFRM1000" size="10.000000pt">to move to the previous day. The window should display in a simple list box the events for the select day (for all the calendars). </font>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642365152" ID="ID_442333272" MODIFIED="1460642723737" TEXT="CRM05.3- Advanced Agenda Window">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 52" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">The agenda window should now have a display area divided in 24 slots, one for each hour of the day. Each of the slots should have a small text displaying the hour of the day. The Events should be displayed in a size corresponding to its duration and in the colour of the calendar. It should be possible to select the calendars to display in the window. When double clicking in an event its edit window should appear. It should be possible to switch between this new view and a view that display the events of the day in a simple list (like CRM05.2). </span></font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460629044415" ID="ID_76498300" MODIFIED="1460629602204">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CRM06- Reminders and Tasks
    </p>
  </body>
</html></richcontent>
<node CREATED="1460642548641" ID="ID_343028349" MODIFIED="1460642713236" TEXT="CRM06.1- Reminders Edition">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 52" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">A new window to create, edit and remove reminders. Reminders are like events but with reminders the user is notified when the due date of the reminder arrives. A reminder has a name, a description and a timestamp (due date). Cleansheets should only allow valid timestamps. The window should list all the existing reminders. When the due date of a reminder is reached Cleansheets should automatically display an alert to the user in a popup window. This popup window should the name, description and due date of the reminder. The window should have two buttons. One button to close the window and the other button to remind again the user in 5 minutes. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642559415" ID="ID_356336711" MODIFIED="1460642705000" TEXT="CRM06.2- Tasks Edition">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 52" class="page">
      <div class="layoutArea">
        <div class="column">
          <ul style="list-style-type: disc">
            <li style="font-family: CMSY10; font-size: 10.000000pt">
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to create, edit and remove tasks. A task has a name, a description and is associated </span></font>
              </p>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">with a contact. A task also has a priority level (1 to 5) and a percentage of completion. </span></font>
              </p>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Cleansheets should have a sidebar window to display tasks. A double click in a task should open a window to edit the task. In the sidebar it should be possible to sort and filter the tasks using expressions based on its fields. For instance, it should be possible to only display tasks that are not completed. </span></font>
              </p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642568700" ID="ID_1814819524" MODIFIED="1460642691568" TEXT="CRM06.3- Tasks, Reminders, Events">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 52" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">Tasks may now have due dates. For tasks with due dates it should be possible to create reminders (one or more). The reminders should have a due date that is earlier than the due date of the task. It should also be possible to create an event in a calendar of the contact to mark the task deadline. For events, tasks, contacts and reminders that are related the user interface should also provide visual information of that relationships and it should be possible to &quot;move&quot; between linked elements (e.g., it should be possible to open a task window from an event that is associated with a task). </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1460629055115" ID="ID_1643792357" MODIFIED="1460629610433">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CRM07- CRM Outputs and Charts
    </p>
  </body>
</html></richcontent>
<node CREATED="1460642581149" ID="ID_310894824" MODIFIED="1460642677862" TEXT="CRM07.1- Labels for Contacts">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 52" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to generate labels for contacts. A PDF document should be generated in which each page has a label of one contact (the library iText should be used for the generation of the PDF - </span></font><font face="SFTT1000" size="10.000000pt"><span style="font-family: SFTT1000; font-size: 10.000000pt">http://developers.itextpdf.com</span></font><font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">). Labels must include at least the name, photograph, addresses, emails and telephone numbers of the contact. The user should also have the option to select if the events of the contacts should be included in the PDF. In this case the user must enter the date interval that is used to select the events. It should be possible to select the contacts using regular expression on the name of the contact and/or selecting a specific town or city. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642591071" ID="ID_805154529" MODIFIED="1460642664433" TEXT="CRM07.2- Export/Import Calendar">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 52" class="page">
      <div class="layoutArea">
        <div class="column">
          <ol style="list-style-type: none" start="0">
            <li>
              <p>
                <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to import and export the contents of the agenda (i.e., one or more calendars) of a contact. All the contents of the calendars should be included in the process. The user should select the contact and the set of calendars to include in the process. On import the source should be an worksheet or a xml file. On export the target should be an worksheet or an xml file. There should be a window to configure the tags to be used in the xml file. </span></font>
              </p>
            </li>
          </ol>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1460642600271" ID="ID_1469512233" MODIFIED="1460642647564" TEXT="CRM07.3- Gantt Chart">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 53" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="SFRM1000" size="10.000000pt"><span style="font-family: SFRM1000; font-size: 10.000000pt">It should be possible to generate a Gantt diagram with the contents of one or more calendars of a contact. The user should select the contact, the calendars and a date interval for the generation of the Gantt chart. The chart should be generated in an worksheet. Columns of the worksheets should represent dates and lines (rows) should represent elements of the calendars (e.g., events). Attention should be given to the &quot;scale&quot; of the chart. For instance, if an event has a duration of two hours and each column represents one hour, then the event should occupy two cells. </span></font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</map>
