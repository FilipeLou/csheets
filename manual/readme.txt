== README ==

1. Requirements

- This manual uses TeXlipse (http://texlipse.sourceforge.net).

- PlantUML is used to generate diagrams. The ant build file has a task that should be executed in order to generate the diagrams. The classpath if the build.xml file should be adapted to the path of the plantuml jar in your system. 

2. Notes about configuration

Make sure eclipse is configured to use encoding UTF8.

Optionally, For Mac OS X configure the terminal to use encoding UTF8 before running eclipse and editing the files of Latex.

In the main file of Latex include the following line:
\usepackage[utf8]{inputenc}

instead of:
\usepackage[applemac]{inputenc}

